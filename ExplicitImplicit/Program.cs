﻿
using System;

namespace ExplicitImplicit
{
    class Program
    {
        static void Main(string[] args)
        {
            C c = new C();
            A a1 = c; //Нормально - есть неявный оператор A
            A a2 = (A)c; //Тоже нормально - неявный оператор может вызываться явно
            B b1 = c; //Ошибка - нет неявного ператора пробразования C->B
            B b2 = (B)c; //Нормально - есть явное преобразование C->B
            Console.Read();
        }
    }
    class A { }
    class B { }
    class C
    {
        public static implicit operator A(C c) { return new A(); }
        public static explicit operator B(C c) { return new B(); }
    }
}
